import React, { useState } from 'react';
import Button from './components/Button/Button';
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';
import './App.scss';
import './index.js'

const App = () => {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  const openFirstModal =
   () => {
    setFirstModalOpen(true);
  };

  const openSecondModal = () => {
    setSecondModalOpen(true);
  };

  const closeFirstModal = () => {
    setFirstModalOpen(false);
  };

  const closeSecondModal = () => {
    setSecondModalOpen(false);
  };

  return (
    <div className="app">
      <div className='containerButton'>
        <Button onClick={openFirstModal}>Open first modal</Button>
        <Button onClick={openSecondModal}>Open second modal</Button>
      </div>

      <ModalImage isOpen={firstModalOpen} onClose={closeFirstModal} />
      <ModalText isOpen={secondModalOpen} onClose={closeSecondModal} />
    </div>
  );
};

export default App;