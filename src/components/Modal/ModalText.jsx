import Modal from './Modal';
import ModalWrapper from './ModalWrapper';
import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';
import ModalBackground from './ModalBackground';
import PropTypes from 'prop-types';

const ModalText = ({ isOpen, onClose }) => {
  if (!isOpen) return null;

  return (
    <>
    <Modal>
      <ModalWrapper>
        <ModalHeader>Add Product “NAME”</ModalHeader>
        <ModalBody>
          <p>Description for you product</p>
        </ModalBody>
        <ModalFooter firstText="ADD TO FAVORITE" firstClick={onClose} />
        <ModalClose onClick={onClose} />
      </ModalWrapper>
    </Modal>
    <ModalBackground click={onClose} />
    </>
  );
};

ModalText.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ModalText;