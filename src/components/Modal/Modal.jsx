import PropTypes from 'prop-types';

const Modal = ({ children, onClose }) => {
  return (
    <div className="modal" onClick={onClose}>
      <div className="modalContent" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.node, 
  onClose: PropTypes.func,
};

export default Modal;